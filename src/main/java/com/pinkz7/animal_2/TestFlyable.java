/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat b1 = new Bat("Bat I");
        Plane p1 = new Plane("Engine number I");
        Dog d1 = new Dog("Too");
        Car c1 = new Car("zXis");
        
        Flyable[]flyables ={p1,b1};
        for(Flyable f: flyables){
            if(f instanceof Plane){
            Plane p = (Plane)f;
            p.startEngine();
            p.run();
            p.raiseSpeed();
        }
            f.fly();
        }
        
        Runable[]runables ={d1,p1,c1};
        for(Runable r : runables){
            r.run();
        }
    }
}
