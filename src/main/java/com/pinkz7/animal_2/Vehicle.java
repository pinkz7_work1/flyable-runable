/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public abstract class Vehicle {
    String engine;
    
    public Vehicle(String engine){
        this.engine=engine;
    }
    
    public abstract void startEngine();
    public abstract void stopEngine();
    public abstract void raiseSpeed();
    public abstract void alppyBreak();
}
