/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public abstract class Reptile extends Animal{
    
    public Reptile(String name, int numberofLeg) {
        super(name, numberofLeg);
    }
    
    public abstract void crawl();
    
}
