/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public class Plane extends Vehicle implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println(engine+" Plane: StartEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println(engine+" Plane: StopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println(engine+" Plane: RaiseSpeed");
    }

    @Override
    public void alppyBreak() {
        System.out.println(engine+" Plane: AlppyBreak");
    }

    @Override
    public void fly() {
        System.out.println(engine+" Plane: Fly");
    }

    @Override
    public void run() {
        System.out.println(engine+" Plane: Run");
    }

}
