/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        Cat c1 = new Cat("Dink");
        Dog d1 = new Dog("Dip");
        Crocodile cr1 = new Crocodile("Dook");
        Snake s1 = new Snake("Derek");
        Fish f1 = new Fish("Darok");
        Crab ca1 = new Crab("Darok");
        Bat b1 = new Bat("Dez");
        Bird bd1 = new Bird("Dezep");
        System.out.println(h1);
        System.out.println(c1);
        System.out.println(d1);
        System.out.println(cr1);
        System.out.println(s1);
        System.out.println(ca1);
        System.out.println(b1);
        System.out.println(bd1);
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        cr1.eat();
        cr1.walk();
        cr1.crawl();
        cr1.speak();
        cr1.sleep();
        s1.eat();
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        f1.eat();
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        ca1.eat();
        ca1.walk();
        ca1.swim();
        ca1.speak();
        ca1.sleep();
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        bd1.eat();
        bd1.walk();
        bd1.fly();
        bd1.speak();
        bd1.sleep();
        Animal[]Animals = {h1,c1,d1,cr1,s1,f1,ca1,b1,bd1};
        for(int i=0; i<Animals.length; i++){
            Animal a1 =Animals[i];
            System.out.println(a1.getNickname()+" instanceof Animal : "+(a1 instanceof Animal));
            System.out.println(a1.getNickname()+" instanceof LandAnimal : "+(a1 instanceof LandAnimal));
            System.out.println(a1.getNickname()+" instanceof AquaticAnimal : "+(a1 instanceof AquaticAnimal));
            System.out.println(a1.getNickname()+" instanceof Poultry : "+(a1 instanceof Poultry));
            System.out.println(a1.getNickname()+" instanceof Reptile : "+(a1 instanceof Reptile));
        }
        
    }
}
