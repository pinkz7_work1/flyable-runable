/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal_2;

/**
 *
 * @author ripgg
 */
public class Car extends Vehicle implements Runable{

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println(engine+" Car: StartEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println(engine+" Car: StopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println(engine+" Car: RaiseSpeed");
    }

    @Override
    public void alppyBreak() {
        System.out.println(engine+" Car: AlppyBreak");
    }

    @Override
    public void run() {
        System.out.println(engine+" Car: Run");
    }
    
}
